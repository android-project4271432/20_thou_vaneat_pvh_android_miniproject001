package com.example.miniproject001.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.miniproject001.R;
import com.example.miniproject001.databinding.CustomGalleryScreenBinding;

import java.util.ArrayList;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryHolder> {
    private final Context context;
    private final ArrayList<String> imageGallery;
    private final ArrayList<String> textGallery;
    private final ArrayList<String> textSmallGallery;

    public GalleryAdapter(Context context, ArrayList<String> imageGallery, ArrayList<String> textGallery, ArrayList<String> textSmallGallery) {
        this.context = context;
        this.imageGallery = imageGallery;
        this.textGallery = textGallery;
        this.textSmallGallery = textSmallGallery;
    }

    @NonNull
    @Override
    public GalleryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_gallery_screen,parent,false);
        return new GalleryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryHolder holder, int position) {

        Glide.with(context)
                .load(imageGallery.get(position))
                .into(holder.galleryImageView);

        holder.galleryTextView.setText(textGallery.get(position));
        holder.gallerySmallTextView.setText(textSmallGallery.get(position));
    }

    @Override
    public int getItemCount() {
        return imageGallery.size();
    }

    public static class GalleryHolder extends RecyclerView.ViewHolder{

        CustomGalleryScreenBinding bindingGallery;
        private final ImageView galleryImageView;
        private final TextView galleryTextView,gallerySmallTextView;
        public GalleryHolder(@NonNull View itemView) {
            super(itemView);
            bindingGallery= CustomGalleryScreenBinding.bind(itemView);
            galleryImageView = bindingGallery.imageGallery;
            galleryTextView=bindingGallery.textGallery;
            gallerySmallTextView=bindingGallery.textSmallGallery;

        }
    }
}

package com.example.miniproject001.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.miniproject001.R;
import com.example.miniproject001.databinding.CustomContactBinding;
import com.example.miniproject001.forclass.ContactNumber;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactHolder> {

    private final Context context;
    private final ArrayList<String> imageContactView;
    private final ArrayList<String> textContact;
    private final ArrayList<String> imageContactPhone;
    private final ContactNumber contactNumber;

    public ContactAdapter(Context context, ArrayList<String> imageContactView, ArrayList<String> textContact, ArrayList<String> imageContactPhone, ContactNumber contactNumber) {
        this.context = context;
        this.imageContactView = imageContactView;
        this.textContact = textContact;
        this.imageContactPhone = imageContactPhone;
        this.contactNumber = contactNumber;
    }

    @NonNull
    @Override
    public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_contact,parent,false);
        return new ContactAdapter.ContactHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactHolder holder, int position) {
        Glide.with(context)
                .load(imageContactView.get(position))
                .into(holder.imageContactView);

        holder.textContact.setText(textContact.get(position));

        holder.imageContactPhone.setOnClickListener(view -> {
            contactNumber.onItemClick(imageContactPhone.get(position));
        });
    }


    @Override
    public int getItemCount() {
        return imageContactPhone.size();
    }

    public class ContactHolder extends RecyclerView.ViewHolder{

        CustomContactBinding bindingContact;

        private final ImageView imageContactView,imageContactPhone;
        private final TextView textContact;
       public ContactHolder(@NonNull View itemView) {
           super(itemView);

           bindingContact=CustomContactBinding.bind(itemView);

           imageContactView = bindingContact.imageContactView;
           textContact = bindingContact.textContact;
           imageContactPhone = bindingContact.imageContactPhone;
       }
    }
}

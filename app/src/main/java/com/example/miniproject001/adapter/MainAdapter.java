package com.example.miniproject001.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.miniproject001.R;
import com.example.miniproject001.databinding.CustomHomescreenBinding;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainHolder> {

    private final Context context;
    private final ArrayList<String> imageArrayList;

    public MainAdapter(Context context, ArrayList<String> integerArrayList) {

        this.context = context;
        this.imageArrayList = integerArrayList;
    }

    @NonNull
    @Override
    public MainHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_homescreen,parent,false);
        return new MainHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainHolder holder, int position) {

        Glide.with(context)
                .load(imageArrayList.get(position))
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public class MainHolder extends RecyclerView.ViewHolder{

        CustomHomescreenBinding bindingHome;

        private final ImageView imageView;
        public MainHolder(@NonNull View itemView) {
            super(itemView);
            bindingHome= CustomHomescreenBinding.bind(itemView);
            imageView = bindingHome.imageHome;
        }
    }
}

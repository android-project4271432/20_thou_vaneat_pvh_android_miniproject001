package com.example.miniproject001.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;

import com.example.miniproject001.R;
import com.example.miniproject001.adapter.GalleryAdapter;
import com.example.miniproject001.adapter.MainAdapter;
import com.example.miniproject001.base.BaseActivity;
import com.example.miniproject001.databinding.ActivityGalleryBinding;
import com.example.miniproject001.databinding.ActivityMainBinding;

import java.util.ArrayList;

public class GalleryActivity extends BaseActivity<ActivityGalleryBinding> {

    private ArrayList<String> imageGallery = new ArrayList<>();
    private ArrayList<String> textGallery = new ArrayList<>();
    private ArrayList<String> textSmallGallery = new ArrayList<>();

    private GalleryAdapter galleryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageGallery.add("https://i.pinimg.com/236x/0f/d5/77/0fd577090fdb87894a9bdcb59066cea8.jpg");
        textGallery.add("Cute Cat");
        textSmallGallery.add("900 views");

        imageGallery.add("https://i.pinimg.com/236x/f2/02/5a/f2025afe7ecd4b05ca5dfd488d4d1283.jpg");
        textGallery.add("Money");
        textSmallGallery.add("150 views");

        imageGallery.add("https://i.pinimg.com/474x/69/7e/3c/697e3cfbe6b87132d5309b679e8e5378.jpg");
        textGallery.add("Beauty Bird ");
        textSmallGallery.add("100 views");

        imageGallery.add("https://i.pinimg.com/236x/46/d4/aa/46d4aa27cad317a2ffc560fd5462d577.jpg");
        textGallery.add("Light");
        textSmallGallery.add("90 views");


        LinearLayoutManager linearLayoutManager = new GridLayoutManager(this,2);
        galleryAdapter = new GalleryAdapter(this,imageGallery,textGallery,textSmallGallery);

        binding.galleryScreenRecycler.setLayoutManager(linearLayoutManager);
        binding.galleryScreenRecycler.setAdapter(galleryAdapter);

    }


    @Override
    public int layout() {
        return R.layout.activity_gallery;
    }
}
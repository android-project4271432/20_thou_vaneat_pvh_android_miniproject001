package com.example.miniproject001.activity;

import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.miniproject001.R;
import com.example.miniproject001.adapter.ContactAdapter;
import com.example.miniproject001.base.BaseActivity;
import com.example.miniproject001.databinding.ActivityContactBinding;
import com.example.miniproject001.forclass.ContactNumber;

import java.util.ArrayList;

public class ContactActivity extends BaseActivity<ActivityContactBinding> {


    private ContactAdapter contactAdapter;
    private ArrayList<String> imageContactView = new ArrayList<>();
    private ArrayList<String> textContact = new ArrayList<>();
    private ArrayList<String> imageContactPhone =new ArrayList<>();
    private ContactNumber contactNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        imageContactView.add("https://i.pinimg.com/236x/df/6d/1d/df6d1d53d5544456ee6375c38016c601.jpg");
        textContact.add("Taylor");
        imageContactPhone.add("0999999");


        imageContactView.add("https://i.pinimg.com/236x/d3/35/ed/d335ed986e57ae5a60c7d7b61690451c.jpg");
        textContact.add("Thimotee");
        imageContactPhone.add("344567");

        imageContactView.add("https://i.pinimg.com/236x/3e/f0/8e/3ef08eaf9742a3d8c267954a7c0da038.jpg");
        textContact.add("Yoya");
        imageContactPhone.add("4456789");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        contactAdapter = new ContactAdapter(this,imageContactView,textContact,imageContactPhone, contactNumber);

        binding.contactScreenRecycler.setLayoutManager(linearLayoutManager);
        binding.contactScreenRecycler.setAdapter(new ContactAdapter(getApplicationContext(), imageContactView, textContact, imageContactPhone, new ContactNumber() {
            @Override
            public void onItemClick(String item) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tell:" + imageContactPhone));
                startActivity(intent);
            }
        }));
        }


    @Override
    public int layout() {
        return R.layout.activity_contact;
    }
}
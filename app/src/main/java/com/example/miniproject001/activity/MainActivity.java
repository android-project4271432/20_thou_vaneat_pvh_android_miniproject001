package com.example.miniproject001.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.miniproject001.R;
import com.example.miniproject001.adapter.MainAdapter;
import com.example.miniproject001.base.BaseActivity;
import com.example.miniproject001.databinding.ActivityMainBinding;
import com.example.miniproject001.databinding.CustomNotebookBinding;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;

public class MainActivity extends BaseActivity<ActivityMainBinding> {

    private ArrayList<String> imageArrayList;
    private MainAdapter mainAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForContextMenu(binding.imgOption);

        //=======================================Home screen===========================================
        imageArrayList = new ArrayList<>();
        imageArrayList.add("https://i.pinimg.com/474x/69/7e/3c/697e3cfbe6b87132d5309b679e8e5378.jpg");
        imageArrayList.add("https://i.pinimg.com/564x/8a/b2/5f/8ab25ff030d8dbb3997dd3608197b086.jpg");
        imageArrayList.add("https://i.pinimg.com/474x/2a/f0/4b/2af04be44642681693c67f01d45fef28.jpg");
        imageArrayList.add("https://i.pinimg.com/474x/71/64/00/7164002df92b4a939f7b7c088ebdd875.jpg");
        imageArrayList.add("https://i.pinimg.com/474x/ff/67/74/ff677496eb33e30186eeebee937b7836.jpg");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        binding.homeScreenRecycler.setLayoutManager(linearLayoutManager);

        mainAdapter = new MainAdapter(this, imageArrayList);
        binding.homeScreenRecycler.setAdapter(mainAdapter);

        //============================================Chip=============================================
        String[] chipMusic = {"Pop music", "Romantic", "Hiphop", "Opera"};
        for (String music : chipMusic) {
            Chip chip = new Chip(this);
            chip.setText(music);
            chip.setChipBackgroundColorResource(R.color.white);
            binding.chipView.addView(chip);
        }

        // ==========================Intent Activity=============================================

        binding.imgGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
                startActivity(intent);
            }
        });

        binding.imgContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(intent1);
            }
        });


        binding.imgNoteBook.setOnClickListener(view -> {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            CustomNotebookBinding bindingNote = CustomNotebookBinding.inflate(LayoutInflater.from(this));
            builder.setView(bindingNote.getRoot());
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        binding.imgAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(MainActivity.this, UserActivity.class);
                startActivity(intent2);
            }
        });
    }


    //=======================================Menu========================================================
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.exit) {
            System.exit(1);
        } else if (item.getItemId() == R.id.cancel) {
            Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
        }
        return MainActivity.super.onContextItemSelected(item);
    }

    //=================================DataBinding==================
    @Override
    public int layout() {
        return R.layout.activity_main;
    }
}
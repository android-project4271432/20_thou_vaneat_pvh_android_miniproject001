package com.example.miniproject001.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.miniproject001.R;
import com.example.miniproject001.base.BaseActivity;
import com.example.miniproject001.databinding.ActivityUserBinding;
import com.example.miniproject001.forclass.User;

public class UserActivity extends BaseActivity<ActivityUserBinding> {

    private String TAG = "User";
    String selectGender, gender;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        binding.genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedGenderId = binding.genderRadioGroup.getCheckedRadioButtonId();
                if (selectedGenderId == -1) {
                    Toast.makeText(getApplicationContext(), "Please select gender", Toast.LENGTH_SHORT).show();
                } else {
                    RadioButton selectedGenderButton = findViewById(selectedGenderId);
                    selectGender = selectedGenderButton.getText().toString();
                }
            }
        });

        binding.btnSave.setOnClickListener(view -> {

            String name = binding.textName.getText().toString();
            String career = binding.textCareer.getText().toString();

            if (name.isEmpty()){
              binding.textName.setError("Name are required");
              binding.textName.startAnimation(shakeError());
            }if (career.isEmpty()) {
                binding.textCareer.setError("Career are required");
                binding.textCareer.startAnimation(shakeError());
            } else {
               user = new User(name,gender,career);
                Intent intent = new Intent(this, UserActivity.class);
                intent.putExtra("user",user);

                if (selectGender.equalsIgnoreCase("Male")){
                    binding.getTextUser.setText("Welcome Mr. " + name);
                }else {
                    binding.getTextUser.setText("Welcome Mrs. " + name);
                }
                binding.getTextCareer.setText("working as " + career);
            }
        });
    }

    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 5, 0, 0);
        shake.setDuration(100);
        shake.setInterpolator(new CycleInterpolator(2));
        return shake;
    }

    @Override
    public int layout() {
        return R.layout.activity_user;
    }
}